module mailgun-sender

go 1.13

require (
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/likexian/whois-go v1.6.0
	github.com/mailgun/mailgun-go/v4 v4.1.0
	github.com/sirupsen/logrus v1.6.0
	github.com/tkanos/gonfig v0.0.0-20181112185242-896f3d81fadf
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
