package mailer

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/likexian/whois-go"
	"github.com/mailgun/mailgun-go/v4"
	log "github.com/sirupsen/logrus"
	"github.com/tkanos/gonfig"
)

type mailgunConfiguration struct {
	Domain    string
	APIKey    string
	Recipient string
	Sender    string
}

type bannedDetails struct {
	IP          string
	Service     string
	WhoIsResult string
}

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	file, err := os.OpenFile("/var/log/mailgun_sender.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)

	if err == nil {
		log.SetOutput(file)
	} else {
		log.SetOutput(os.Stderr)
		log.WithFields(log.Fields{
			"Error": err,
		}).Warn("Not able to create log file. Writing to stderr")
	}

	// Only log the warning severity or above.
	log.SetLevel(log.WarnLevel)
}

// Send sends an email via MailGun
func Send(ipAddress string, service string, confFile string) {
	banned := bannedDetails{IP: ipAddress,
		Service: service}

	mailgunConfiguration := loadMailgunConfiguration(confFile)

	result, err := whois.Whois(ipAddress)

	if err == nil {
		banned.WhoIsResult = result
	} else {
		banned.WhoIsResult = "No info found. See Logs"
		log.WithFields(log.Fields{
			"ip":    ipAddress,
			"Error": err,
		}).Warn("Not able to include WHOIS information in email.")
	}

	sendMailgunTemplate(banned, mailgunConfiguration)
}

func loadMailgunConfiguration(confFile string) mailgunConfiguration {
	mailgunConfiguration := mailgunConfiguration{}

	if _, err := os.Stat(confFile); os.IsNotExist(err) {
		log.WithFields(log.Fields{
			"configurationFile": confFile,
			"Error":             err,
		}).Fatal("The configuration file does not exist")
	}

	err := gonfig.GetConf(confFile, &mailgunConfiguration)

	if err != nil {
		log.WithFields(log.Fields{
			"Error":             err,
			"configurationFile": confFile,
		}).Fatal("Could not load configurations from file.")
	}

	return mailgunConfiguration
}

func sendMailgunTemplate(banned bannedDetails, mailgunConfiguration mailgunConfiguration) {
	mg := mailgun.NewMailgun(mailgunConfiguration.Domain, mailgunConfiguration.APIKey)

	sender := mailgunConfiguration.Sender
	subject := fmt.Sprintf("%s banned from %s", banned.IP, banned.Service)
	body := ""
	recipient := mailgunConfiguration.Recipient

	message := mg.NewMessage(sender, subject, body, recipient)
	message.SetTemplate("fail2ban_banned")
	message.AddTemplateVariable("ip_address", banned.IP)
	message.AddTemplateVariable("service", banned.Service)
	message.AddTemplateVariable("whois", banned.WhoIsResult)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	resp, id, err := mg.Send(ctx, message)

	if err != nil {
		log.WithFields(log.Fields{
			"Error":    err,
			"Response": resp,
		}).Fatal("Message not sent. Failed to send with Mailgun")
	}

	log.WithFields(log.Fields{
		"MessageId": id,
		"Response":  resp,
	}).Info("Fail2Ban message sent")
}
