package main

import (
	"flag"
	"mailgun-sender/mailer"
	"os"
)

func main() {

	var ipAddress string
	var service string
	var confFile string

	flag.StringVar(&ipAddress, "ip-address", "", "Banned IP address")
	flag.StringVar(&service, "service", "", "Service IP is banned from")
	flag.StringVar(&confFile, "conf", "mailgun_conf.json", "Location of conf file (DEFAULT: expects mailgun_conf.json in same directory")
	flag.Parse()

	if ipAddress == "" || service == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	mailer.Send(ipAddress, service, confFile)
}
